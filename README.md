# HOW TO DEVELOP A KEYCLOAK THEME

* clone this repository
* disable theme caching in keycloak's config file `standalone/configuration/standalone-ha.xml`:

```xml
<!-- 
<staticMaxAge>2592000</staticMaxAge>
<cacheThemes>true</cacheThemes>
<cacheTemplates>true</cacheTemplates> 
-->
<staticMaxAge>-1</staticMaxAge>
<cacheThemes>false</cacheThemes>
<cacheTemplates>false</cacheTemplates> 
```

* start rnback dashboard and keycloak instances
* make your changes
* override keycloak's themes dir with updated themes dir:

```bash
# inside the repository directory, assuming keycloak's container is named 'keycloak'
docker cp themes/ keycloak:/opt/jboss/keycloak/
```

* refresh the website in the browser

# MORE INFO

* [keycloak docs](http://www.keycloak.org/docs/3.3/server_development/topics/themes.html)
* [keycloak themes examples (github)](https://github.com/keycloak/keycloak/tree/master/examples/themes)
* [FreeMarker docs](http://freemarker.org/docs/pgui_quickstart.html) 

# minify:

```bash
minify themes/rnbackLoginDefault/login/resources/js/rnbackAnalytics.js -o themes/rnbackLoginDefault/login/resources/js/rnbackAnalytics.min.js
```
