if (document.location.hostname !== 'localhost') {

    !function () {
        var analytics = window.analytics = window.analytics || [];
        if (!analytics.initialize) if (analytics.invoked) window.console && console.error && console.error("Segment snippet included twice."); else {
            analytics.invoked = !0;
            analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"];
            analytics.factory = function (t) {
                return function () {
                    var e = Array.prototype.slice.call(arguments);
                    e.unshift(t);
                    analytics.push(e);
                    return analytics
                }
            };
            for (var t = 0; t < analytics.methods.length; t++) {
                var e = analytics.methods[t];
                analytics[e] = analytics.factory(e)
            }
            analytics.load = function (t) {
                var e = document.createElement("script");
                e.type = "text/javascript";
                e.async = !0;
                e.src = ("https:" === document.location.protocol ? "https://" : "http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js";
                var n = document.getElementsByTagName("script")[0];
                n.parentNode.insertBefore(e, n)
            };
            analytics.SNIPPET_VERSION = "4.0.0";
            analytics.load("8YiuIcRs8JNbu7O9Nk9E60kx58iYxJ3t");
            analytics.page();
        }
    }();

    window.onload = function () {
        analytics.ready(function () {
            var waited = false;

            var registrationForm = document.getElementById('kc-register-form');

            function registrationAnalyticsListener(ev) {
                if (waited) {
                    return true;
                }
                var email = document.getElementById('email').value;
                analytics.alias && analytics.alias(email);
                analytics.flush && analytics.flush();
                analytics.identify && analytics.identify(email);
                analytics.track && analytics.track('Signed Up');
                waited = true;
                setTimeout(function () {
                    document.getElementById('kc-register-form').submit()
                }, 1000);
                return false;
            }

            if (registrationForm) {
                registrationForm.onsubmit = registrationAnalyticsListener;
            }

            var loginForm = document.getElementById('kc-form-login');

            function loginAnalyticsListener(ev) {
                if (waited) {
                    return true;
                }
                var email = document.getElementById('username').value;
                analytics.alias && analytics.alias(email);
                analytics.flush && analytics.flush();
                analytics.identify && analytics.identify(email);
                analytics.track && analytics.track('Signed In');
                waited = true;
                setTimeout(function () {
                    document.getElementById('kc-form-login').submit()
                }, 1000);
                return false;
            }

            if (loginForm) {
                loginForm.onsubmit = loginAnalyticsListener;
            }
        });
    };

}
