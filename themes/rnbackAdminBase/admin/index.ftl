<!DOCTYPE html>
<html>
<head>
    <title></title>

    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="${resourceUrl}/rnback/images/favicon.ico">
<#if properties.styles?has_content>
    <#list properties.styles?split(' ') as style>
        <link href="${resourceUrl}/${style}" rel="stylesheet"/>
    </#list>
</#if>
    <#-- rnback css -->
    <link rel="stylesheet" href="${resourceUrl}/rnback/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${resourceUrl}/rnback/css/font-mfizz.min.css"/>
    <link rel="stylesheet" href="${resourceUrl}/rnback/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="${resourceUrl}/rnback/css/nprogress.min.css"/>
    <link rel="stylesheet" href="${resourceUrl}/rnback/css/animate.min.css"/>
    <link rel="stylesheet" href="${resourceUrl}/rnback/css/custom.min.css"/>
    <#-- rnback css end -->

    <script type="text/javascript">
        var authUrl = '${authUrl}';
        var consoleBaseUrl = '${consoleBaseUrl}';
        var resourceUrl = '${resourceUrl}';
        var masterRealm = '${masterRealm}';
    </script>

    <!-- Minimized versions (for those that have one) -->
    <script src="${resourceUrl}/node_modules/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="${resourceUrl}/node_modules/select2/select2.js" type="text/javascript"></script>
    <script src="${resourceUrl}/node_modules/angular/angular.min.js"></script>
    <script src="${resourceUrl}/node_modules/angular-resource/angular-resource.min.js"></script>
    <script src="${resourceUrl}/node_modules/angular-route/angular-route.min.js"></script>
    <script src="${resourceUrl}/node_modules/angular-cookies/angular-cookies.min.js"></script>
    <script src="${resourceUrl}/node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="${resourceUrl}/node_modules/angular-translate/dist/angular-translate.min.js"></script>
    <script src="${resourceUrl}/node_modules/angular-translate-loader-url/angular-translate-loader-url.min.js"></script>
    <script src="${resourceUrl}/node_modules/angular-ui-select2/src/select2.js" type="text/javascript"></script>
    <script src="${resourceUrl}/node_modules/autofill-event/autofill-event.js"></script>


    <!-- Unminimized versions
    <script src="${resourceUrl}/node_modules/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="${resourceUrl}/node_modules/select2/select2.js" type="text/javascript"></script>
    <script src="${resourceUrl}/node_modules/angular/angular.js"></script>
    <script src="${resourceUrl}/node_modules/angular-resource/angular-resource.js"></script>
    <script src="${resourceUrl}/node_modules/angular-route/angular-route.js"></script>
    <script src="${resourceUrl}/node_modules/angular-cookies/angular-cookies.js"></script>
    <script src="${resourceUrl}/node_modules/angular-sanitize/angular-sanitize.js"></script>
    <script src="${resourceUrl}/node_modules/angular-translate/dist/angular-translate.js"></script>
    <script src="${resourceUrl}/node_modules/angular-translate-loader-url/angular-translate-loader-url.js"></script>
    <script src="${resourceUrl}/node_modules/angular-ui-select2/src/select2.js" type="text/javascript"></script>
    <script src="${resourceUrl}/node_modules/autofill-event/autofill-event.js"></script>
    -->

    <!-- Libraries not managed by yarn -->
    <script src="${resourceUrl}/lib/angular/ui-bootstrap-tpls-0.11.0.js"></script>
    <script src="${resourceUrl}/lib/angular/treeview/angular.treeview.js"></script>
    <script src="${resourceUrl}/lib/fileupload/angular-file-upload.min.js"></script>
    <script src="${resourceUrl}/lib/filesaver/FileSaver.js"></script>
    <script src="${resourceUrl}/lib/ui-ace/min/ace.js"></script>
    <script src="${resourceUrl}/lib/ui-ace/ui-ace.min.js"></script>

    <script src="${authUrl}/js/${resourceVersion}/keycloak.js" type="text/javascript"></script>

    <script src="${resourceUrl}/js/app.js" type="text/javascript"></script>
    <script src="${resourceUrl}/js/controllers/realm.js" type="text/javascript"></script>
    <script src="${resourceUrl}/js/controllers/clients.js" type="text/javascript"></script>
    <script src="${resourceUrl}/js/controllers/users.js" type="text/javascript"></script>
    <script src="${resourceUrl}/js/controllers/groups.js" type="text/javascript"></script>
    <script src="${resourceUrl}/js/loaders.js" type="text/javascript"></script>
    <script src="${resourceUrl}/js/services.js" type="text/javascript"></script>

    <!-- Authorization -->
    <script src="${resourceUrl}/js/authz/authz-app.js" type="text/javascript"></script>
    <script src="${resourceUrl}/js/authz/authz-controller.js" type="text/javascript"></script>
    <script src="${resourceUrl}/js/authz/authz-services.js" type="text/javascript"></script>

<#if properties.scripts?has_content>
    <#list properties.scripts?split(' ') as script>
        <script type="text/javascript" src="${resourceUrl}/${script}"></script>
    </#list>
</#if>
</head>
<body class="nav-md" style="height:100%;" data-ng-controller="GlobalCtrl" data-ng-cloak data-ng-show="auth.user">
<div style="height:100%;">
    <div class="container body" style="height:100%;">
        <div class="main_container" style="height:100%;">
            <div class="col-md-3 left_col">
                <div class="left_col menu_fixed">
                    <div class="navbar nav_title" style="border:0;"><a href="https://dashboard.rnback.com/my-backends"
                                                                       class="site_title"><img
                            src="${resourceUrl}/rnback/images/icon-rnback.svg" alt="rnback" height="60px"
                            width="60px"/><span>rnback</span></a></div>
                    <div class="clearfix"></div>
                    <div class="profile clearfix">
                        <div class="profile_pic"></div>
                        <div class="profile_info"><span>Welcome,</span>
                            <h2>
                                <#--TODO insrert name-->
                            </h2></div>
                        <div class="clearfix"></div>
                    </div>
                    <br/>
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section"><h3>general</h3>
                            <ul class="nav side-menu">
                                <li><a href="https://dashboard.rnback.com/my-backends">
                                    <i class="fa fa-gears"></i> backends
                                </a></li>
                                <li><a href="https://dashboard.rnback.com/my-functions">
                                    <i class="fa fa-edit"></i> functions
                                </a></li>
                                <li><a href="https://dashboard.rnback.com/my-users">
                                    <i class="fa fa-users"></i> users
                                </a></li>
                                <li><a href="https://dashboard.rnback.com/my-pg-dbs">
                                    <i class="fa icon-postgres-alt"></i> postgresql
                                </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sidebar-footer hidden-small"><a data-toggle="tooltip" data-placement="top"
                                                                href="https://rnback.com/"><span
                            class="glyphicon glyphicon-home" aria-hidden="true"></span></a><a data-toggle="tooltip"
                                                                                              data-placement="top"
                                                                                              href="https://dashboard.rnback.com/account-settings"><span
                            class="glyphicon glyphicon-user" aria-hidden="true"></span></a><a data-toggle="tooltip"
                                                                                              data-placement="top"
                                                                                              href="javascript:toggleFullScreen();"><span
                            class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span></a><a
                            data-toggle="tooltip" data-placement="top" href="https://dashboard.rnback.com/logout"><span
                            class="glyphicon glyphicon-off" aria-hidden="true"></span></a></div>
                </div>
            </div>
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle"><a id="menu_toggle"><i class="fa fa-bars"></i></a></div>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="true">
                                <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class=" fa fa-angle-down">
                                    <#--TODO insert name-->
                                </span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="https://dashboard.rnback.com/account-settings">account Settings</a>
                                    </li>
                                    <li><a href="https://docs.rnback.com/">help (docs)</a></li>
                                    <li><a href="https://dashboard.rnback.com/logout"><i
                                            class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="right_col" role="main" style="min-height:100%;height:100%;max-width: 100%">
            <#-- original keycloak body content -->

                <div class="container-fluid">
                    <div class="row">
                        <div data-ng-view id="view"></div>
                    </div>
                </div>

                <div class="feedback-aligner" data-ng-show="notification.display">
                    <div class="alert alert-{{notification.type}} alert-dismissable">
                        <button type="button" class="close" data-ng-click="notification.remove()"
                                id="notification-close">
                            <span class="pficon pficon-close"/>
                        </button>

                        <span class="pficon pficon-ok" ng-show="notification.type == 'success'"></span>
                        <span class="pficon pficon-info" ng-show="notification.type == 'info'"></span>
                        <span class="pficon-layered" ng-show="notification.type == 'danger'">
            <span class="pficon pficon-error-octagon"></span>
            <span class="pficon pficon-error-exclamation"></span>
        </span>
                        <span class="pficon-layered" ng-show="notification.type == 'warning'">
            <span class="pficon pficon-warning-triangle"></span>
            <span class="pficon pficon-warning-exclamation"></span>
        </span>
                        <strong>{{notification.header}}</strong> {{notification.message}}
                    </div>
                </div>

                <div id="loading" class="loading">Loading...</div>

            <#-- original keycloak body content end -->
            </div>
            <footer>
                <div class="pull-right">rnback ©</div>
                <div class="clearfix"></div>
                <div style="color:aliceblue;text-align:center;position:fixed;left:25%;bottom:0px;height:30px;width:50%;background:#999;z-index:999;">
                    please note this project is currently in Alpha
                </div>
            </footer>
        </div>
    </div>
    <script src="${resourceUrl}/rnback/js/jquery-2.2.4.min.js"></script>
    <script src="${resourceUrl}/rnback/js/bootstrap.min.js"></script>
    <script src="${resourceUrl}/rnback/js/fastclick.min.js"></script>
    <script src="${resourceUrl}/rnback/js/nprogress.min.js"></script>
    <script src="${resourceUrl}/rnback/js/custom.min.js"></script>
    <script src="${resourceUrl}/rnback/js/rnback.js"></script>
    <script src="${resourceUrl}/rnback/js/crisp.js"></script>
</div>
</body>
</html>